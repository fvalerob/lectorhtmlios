//
//  ViewController.swift
//  LectorHtml
//
//  Created by Antonio Pertusa on 26/10/16.
//  Copyright © 2016 Antonio Pertusa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textField: UITextField!
    
    @IBAction func connectUrl(_ sender: Any) {
        
        self.textField.resignFirstResponder() // Para ocultar el teclado
    
        // TODO: Realizar la conexión y mostrar el resultado de la url
        
        // 1. Crear la sesión
        // 2. Crear URL para conectar a self.textField.text
        // 3- Codificar la URL
        // 4. Crear petición a la URL
        // 5. Establecer conexión
        // 6. Guardar los resultados en el textView (si el texto es muy largo puede que no aparezca, pero no es un problema)
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

